import React, { Component } from "react";
import AppNavigator from "./src/Routes";

export default class App extends Component {
  render() {
    return <AppNavigator />;
  }
}
