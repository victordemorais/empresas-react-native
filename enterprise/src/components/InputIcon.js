import React from "react";
import { StyleSheet, View, TextInput, Text } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

export default props => {
  return (
    <View style={styles.container}>
      <View
        style={[
          styles.containerInput,
          props.style,
          props.error
            ? props.value == ""
              ? {}
              : { borderColor: "red" }
            : props.value == ""
            ? {}
            : { borderColor: "green" }
        ]}
      >
        <Icon name={props.icon} size={20} style={styles.icon} />
        <TextInput {...props} style={styles.input} />
      </View>
      <Text style={styles.error}>{props.value == "" ? "" : props.error}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "column"
  },
  containerInput: {
    width: "100%",
    height: 50,
    backgroundColor: "#eee",
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 2,
    borderWidth: 1,
    fontSize: 80
  },
  icon: {
    color: "#333",
    marginLeft: 20
  },
  input: {
    marginLeft: 20,
    width: "80%"
  },
  error: {
    marginTop: -4,
    color: "red",
    marginBottom: 4
  }
});
