import Config from 'react-native-config'
import React, { Component } from 'react'
import { View, StyleSheet, Text, Image, Dimensions } from 'react-native'
const { width } = Dimensions.get('window')
export default class ListIcon extends Component {
    viewEnterprise = () => {
        this.props.viewEnterprise()
    }
    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={styles.image}
                    source={{ uri: `${Config.URL_API}${this.props.image}` }}
                />

                <Text style={styles.title}>{this.props.title}</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        borderRadius: 3,
        margin: 10,
        shadowRadius: 2,
        shadowOpacity: 0.1,
        shadowOffset: { x: 0, y: 0 },
        width: (width - 45) / 2,
        alignItems: 'center',
    },
    image: {
        width: '100%',
        height: 100,
    },
    title: {
        fontFamily: 'Lato',
        fontSize: 16,
        fontWeight: 'bold',
    },
})
