import { TextInput } from 'react-native'
import React, { Component } from 'react'
import {
    View,
    Button,
    FlatList,
    ActivityIndicator,
    StyleSheet,
    TouchableOpacity,
    Text,
} from 'react-native'
import Enterprise from '../../services/Enterprise'
import Auth from '../../services/Auth'
import ListIcon from '../../components/enterprise/ListIcon'

import Icon from 'react-native-vector-icons/FontAwesome'

export default class Login extends Component {
    state = {
        enterprises: null,
        isLoading: true,
        search: null,
    }
    allEnterprises = async () => {
        await Enterprise.all().then(res => {
            this.setState({ enterprises: res, isLoading: false })
        })
    }
    viewEnterprise = id => {
        this.props.navigation.navigate('Details', { id: id })
    }
    logoutUser = () => {
        Auth.logout()
        this.props.navigation.navigate('Login')
    }
    search = async value => {
        await Enterprise.search(this.state.search).then(res => {
            this.setState({ enterprises: res, isLoading: false })
        })
    }
    componentDidMount() {
        this.allEnterprises()
    }
    render() {
        return this.state.isLoading ? (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <ActivityIndicator size="large" color="#09f" animating />
            </View>
        ) : (
            <View style={styles.container}>
                <View>
                    <TextInput
                        style={styles.input}
                        placeholder="Digite o nome da empresa"
                        onChangeText={search =>
                            this.setState({ search }, () => this.search())
                        }
                    />
                </View>
                <FlatList
                    contentContainerStyle={styles.list}
                    data={this.state.enterprises}
                    numColumns={2}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                            onPress={() => this.viewEnterprise(item.id)}
                        >
                            <View>
                                <ListIcon
                                    title={item.enterprise_name}
                                    image={item.photo}
                                />
                            </View>
                        </TouchableOpacity>
                    )}
                    keyExtractor={item => item.enterprise_name}
                />
                <TouchableOpacity
                    style={styles.logout}
                    onPress={this.logoutUser}
                >
                    <Text>
                        <Icon name={'sign-out'} size={30} color="red" />
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    list: {
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    input: {
        backgroundColor: '#fff',
    },
    logout: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 100,
        elevation: 1,
    },
})
