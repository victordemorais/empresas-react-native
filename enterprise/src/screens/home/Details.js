import React, { Component } from 'react'
import { View, StyleSheet, Text, ActivityIndicator, Image } from 'react-native'
import Enterprise from '../../services/Enterprise'
import Config from 'react-native-config'

export default class Details extends Component {
    state = {
        enterprise: null,
        isLoading: true,
    }
    detail = async () => {
        const id = this.props.navigation.getParam('id')
        await Enterprise.detail(id).then(res => {
            this.setState({ enterprise: res, isLoading: false })
        })
    }
    componentDidMount = () => {
        this.detail()
    }
    render() {
        return this.state.isLoading ? (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <ActivityIndicator size="large" color="#09f" animating />
            </View>
        ) : (
            <View style={styles.container}>
                <Image
                    source={{
                        uri: `${Config.URL_API}${this.state.enterprise.photo}`,
                    }}
                    style={styles.image}
                />

                <View style={styles.description}>
                    <Text style={styles.texts}>
                        <Text style={{ fontWeight: 'bold' }}>
                            Nome da Empresa:
                        </Text>
                        {this.state.enterprise.enterprise_name}
                    </Text>
                    <Text style={styles.texts}>
                        <Text style={{ fontWeight: 'bold' }}>Descrição: </Text>
                        {this.state.enterprise.description}
                    </Text>
                    <Text style={styles.texts}>
                        <Text style={{ fontWeight: 'bold' }}>
                            Tipo de Empresa:
                        </Text>
                        {
                            this.state.enterprise.enterprise_type
                                .enterprise_type_name
                        }
                    </Text>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    image: {
        width: '100%',
        height: 200,
        borderWidth: 1,
        borderRadius: 3,
    },
    description: {
        margin: 20,
    },
    texts: {
        fontSize: 16,
        fontFamily: 'Lato',
    },
})
