import React, { Component } from 'react'
import {
    ImageBackground,
    Image,
    View,
    Button,
    StyleSheet,
    Alert,
} from 'react-native'
import Auth from '../../services/Auth'
import Background from '../../../assets/imgs/background.jpg'
import Logo from '../../../assets/imgs/logo_ioasys.png'
import InputIcon from '../../components/InputIcon'

import { Formik } from 'formik'
import * as Yup from 'yup'

const signin = Yup.object().shape({
    email: Yup.string()
        .email('Email Inválido')
        .required('Campo Obrigatório'),
    password: Yup.string()
        .min(6, 'Senha Curta')
        .required('Campo Obrigatório'),
})
const formValues = { email: '', password: '' }
export default class Login extends Component {
    login = values => {
        Auth.signin(values).then(res => {
            res
                ? this.props.navigation.navigate('Home')
                : Alert.alert(
                      'Tente Novamente!',
                      'Usuário ou senha incorretos.',
                      [{ text: 'Fechar' }]
                  )
        })
    }

    render() {
        return (
            <ImageBackground source={Background} style={styles.background}>
                <View style={styles.container}>
                    <Image source={Logo} />
                    <Formik
                        initialValues={formValues}
                        validationSchema={signin}
                        onSubmit={this.login}
                    >
                        {({ handleChange, handleSubmit, values, errors }) => (
                            <View style={styles.form}>
                                <InputIcon
                                    icon="at"
                                    placeholder="E-mail"
                                    value={values.email}
                                    onChangeText={handleChange('email')}
                                    error={errors.email}
                                />
                                <InputIcon
                                    icon="lock"
                                    secureTextEntry={true}
                                    placeholder="Senha"
                                    value={values.password}
                                    onChangeText={handleChange('password')}
                                    error={errors.password}
                                    maxLength={10}
                                />
                                <Button onPress={handleSubmit} title="Entrar" />
                            </View>
                        )}
                    </Formik>
                </View>
            </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({
    background: {
        flex: 1,
    },
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: 'rgba(0,0,0,.2)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    form: {
        width: '90%',
    },
})
