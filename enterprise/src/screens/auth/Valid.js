import React, { Component } from 'react'
import { View, ActivityIndicator, StyleSheet, AsyncStorage } from 'react-native'
import Auth from '../../services/Auth'

export default class Valid extends Component {
    componentWillMount = () => {
        Auth.check().then(res => {
            res
                ? this.props.navigation.navigate('Home')
                : this.props.navigation.navigate('Login')
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator size="large" />
            </View>
        )
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
})
