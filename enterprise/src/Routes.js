import { createStackNavigator } from 'react-navigation'
import Login from './screens/auth/Login'
import Home from './screens/home/Home'
import Details from './screens/home/Details'
import Valid from './screens/auth/Valid'

const Routes = {
    Loading: {
        screen: Valid,
        navigationOptions: {
            header: null,
        },
    },
    Login: {
        screen: Login,
        navigationOptions: {
            header: null,
        },
    },
    Home: {
        screen: Home,
        navigationOptions: {
            header: null,
        },
    },
    Details: {
        screen: Details,
    },
}
const AppNavigator = createStackNavigator(Routes, {
    initialRouteName: 'Loading',
})

export default AppNavigator
