import { AsyncStorage } from 'react-native'
import Config from 'react-native-config'
import axios from 'axios'

class Auth {
    signin = async ({ email, password }) => {
        try {
            return await axios
                .post(`${Config.URL_API}/api/v1/users/auth/sign_in`, {
                    email,
                    password,
                })
                .then(function(response) {
                    const userData = {
                        token: response.headers['access-token'],
                        client: response.headers.client,
                        uid: response.headers.uid,
                    }
                    AsyncStorage.setItem('userData', JSON.stringify(userData))
                    axios.defaults.headers.common['access-token'] =
                        userData.token
                    axios.defaults.headers.common['uid'] = userData.uid
                    axios.defaults.headers.common['client'] = userData.client
                    return true
                })
                .catch(function(error) {
                    return false
                })
        } catch (err) {
            return false
        }
    }

    check = async () => {
        return await AsyncStorage.getItem('userData')
            .then(data => {
                data = JSON.parse(data)
                axios.defaults.headers.common['access-token'] = data.token
                axios.defaults.headers.common['uid'] = data.uid
                axios.defaults.headers.common['client'] = data.client
                return true
            })
            .catch(err => {
                return false
            })
    }
    logout = () => {
        delete axios.defaults.headers.common['access-token']
        delete axios.defaults.headers.common['uid']
        delete axios.defaults.headers.common['client']
        AsyncStorage.removeItem('userData')
    }
}

export default new Auth()
