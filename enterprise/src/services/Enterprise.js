import axios from 'axios'
import Config from 'react-native-config'
class Enterprise {
    all = async () => {
        try {
            return await axios
                .get(`${Config.URL_API}/api/v1/enterprises`)
                .then(response => {
                    return response.data.enterprises
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            return null
        }
    }
    detail = async id => {
        try {
            return await axios
                .get(`${Config.URL_API}/api/v1/enterprises/${id}`)
                .then(response => {
                    return response.data.enterprise
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            return null
        }
    }
    search = async value => {
        try {
            return await axios
                .get(`${Config.URL_API}/api/v1/enterprises/?name=${value}`)
                .then(response => {
                    return response.data.enterprises
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (err) {
            return null
        }
    }
}
export default new Enterprise()
